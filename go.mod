module gitlab.com/blintmester/mc_buzerator

replace gitlab.com/blintmester/mc_buzerator => ./

go 1.15

require (
	github.com/msteinert/pam v0.0.0-20201130170657-e61372126161
	gopkg.in/yaml.v2 v2.4.0
)

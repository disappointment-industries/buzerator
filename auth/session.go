package auth

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

const filename = "auth/data.json"
const Cookiename = "uid"

var cookies = make(map[string]string)

func AddCookie(cook string, uname string) {
	cookies[cook] = uname
	SaveCookies()
}

func GetUserName(r *http.Request) string {
	if c, e := r.Cookie(Cookiename); e == nil {
		id := c.Value

		if name, ok := cookies[id]; ok {
			return name
		}
	}
	return ""
}

func SaveCookies() {
	b, e := json.MarshalIndent(cookies, "", "  ")
	if e != nil {
		fmt.Println(e)
		return
	}
	e = ioutil.WriteFile(filename, b, os.ModePerm)
	if e != nil {
		fmt.Println(e)
	}
}

func LoadCookies() {
	b, e := ioutil.ReadFile(filename)
	if e != nil {
		fmt.Println(e)
		return
	}
	e = json.Unmarshal(b, &cookies)
	if e != nil {
		fmt.Println(e)
	}
}

func LogoutHandler(w http.ResponseWriter, r *http.Request) {
	if c, e := r.Cookie(Cookiename); e == nil {
		id := c.Value

		if _, ok := cookies[id]; ok {
			delete(cookies, id)
			http.Redirect(w, r, "/", http.StatusFound)
		}
	}
}

func init() {
	LoadCookies()
}

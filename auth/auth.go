package auth

import (
	"errors"
	"github.com/msteinert/pam"
)

func Authenticate(username, password string) bool {
	t, err := pam.StartFunc("login", username, func(s pam.Style, msg string) (string, error) {
		switch s {
		case pam.PromptEchoOff:
			return password, nil
		}
		return "", errors.New("unrecognized message style")
	})
	if err != nil {
		return false
	}
	err = t.Authenticate(0)
	if err != nil {
		return false
	}

	return true
}

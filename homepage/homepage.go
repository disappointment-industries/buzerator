package homepage

import (
	"fmt"
	"gitlab.com/blintmester/mc_buzerator/auth"
	"gopkg.in/yaml.v2"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"time"
)

const filename = "config/services.yml"

type Service struct {
	Name         string
	Running      bool
	LastModified time.Time
	ModifiedBy   string
}

func (s *Service) Start(username string) {
	b, e := exec.Command("systemctl", "start", s.Name).CombinedOutput()
	if e != nil {
		fmt.Println(string(b))
	}
	s.LastModified = time.Now()
	s.ModifiedBy = username
	s.Running = true
}

func (s *Service) Stop(username string) {
	b, e := exec.Command("systemctl", "stop", s.Name).CombinedOutput()
	if e != nil {
		fmt.Println(string(b))
	}
	s.LastModified = time.Now()
	s.ModifiedBy = username
	s.Running = false
}

func (s *Service) PrintTime() string {
	return s.LastModified.Format("Mon Jan 2 15:04:05")
}

type data struct {
	Username string
	Services []Service
}

var templ *template.Template

var Services = make ([]Service, 0)

func init() {
	LoadFile()
	var err error
	templ, err = template.ParseFiles("homepage/index.html")
	if err != nil {
		panic(err)
	}
}

func Handler(w http.ResponseWriter, r *http.Request) {
	d := &data{
		Username: auth.GetUserName(r),
		Services: Services,
	}

	err := templ.Execute(w, d)
	if err != nil {
		fmt.Println(err)
	}
}

func StopHandler(w http.ResponseWriter, r *http.Request) {

	if auth.GetUserName(r) == "" {
		http.Redirect(w, r, "/login", http.StatusFound)
		return
	}

	id := r.URL.Path[1:]
	for i := range Services {
		if Services[i].Name == id {
			Services[i].Stop(auth.GetUserName(r))
		}
	}
	Save()
	http.Redirect(w, r, "/", http.StatusFound)
}

func StartHandler(w http.ResponseWriter, r *http.Request) {
	if auth.GetUserName(r) == "" {
		http.Redirect(w, r, "/login", http.StatusFound)
		return
	}
	id := r.URL.Path[1:]
	for i := range Services {
		if Services[i].Name == id {
			Services[i].Start(auth.GetUserName(r))
		}
	}
	Save()
	http.Redirect(w, r, "/", http.StatusFound)
}

func Save() {
	b, e := yaml.Marshal(Services)
	if e != nil {
		fmt.Println(e)
		return
	}
	e = ioutil.WriteFile(filename, b, os.ModePerm)
	if e != nil {
		fmt.Println(e)
	}
}

func LoadFile() {
	b, e := ioutil.ReadFile(filename)
	if e != nil {
		fmt.Println(e)
		return
	}
	e = yaml.Unmarshal(b, &Services)
	if e != nil {
		fmt.Println(e)
	}
}

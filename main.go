package main

import (
	"fmt"
	"gitlab.com/blintmester/mc_buzerator/auth"
	"gitlab.com/blintmester/mc_buzerator/homepage"
	"gitlab.com/blintmester/mc_buzerator/login"
	"net/http"
)

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/login", login.Handler)
	mux.HandleFunc("/logout/", auth.LogoutHandler)
	mux.Handle("/stop/", http.StripPrefix("/stop", http.HandlerFunc(homepage.StopHandler)))
	mux.Handle("/start/", http.StripPrefix("/start", http.HandlerFunc(homepage.StartHandler)))
	mux.HandleFunc("/", homepage.Handler)

	e := http.ListenAndServe(":6969", mux)
	if e != nil {
		fmt.Println(e)
	}
}

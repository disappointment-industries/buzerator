package login

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"gitlab.com/blintmester/mc_buzerator/auth"
	"html/template"
	"net/http"
)

type data struct {
	LoggedIn     bool
	LoginFailure bool
}

var templ *template.Template

func init() {
	var err error
	templ, err = template.ParseFiles("login/login.html")
	if err != nil {
		panic(err)
	}
}

func GenerateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	// Note that err == nil only if we read len(b) bytes.
	if err != nil {
		return nil, err
	}

	return b, nil
}

// GenerateRandomString returns a URL-safe, base64 encoded
// securely generated random string.
// It will return an error if the system's secure random
// number generator fails to function correctly, in which
// case the caller should not continue.
func GenerateRandomString(s int) (string, error) {
	b, err := GenerateRandomBytes(s)
	return base64.URLEncoding.EncodeToString(b), err
}

func Handler(w http.ResponseWriter, r *http.Request) {
	d := &data{
		LoggedIn: false,
	}

	if r.Method == http.MethodPost {
		err := r.ParseForm()
		if err != nil {
			fmt.Println(err)
			return
		}

		username := r.Form.Get("username")
		password := r.Form.Get("password")

		if auth.Authenticate(username, password) {
			// Example: this will give us a 44 byte, base64 encoded output
			token, err := GenerateRandomString(32)
			if err != nil {
				http.Redirect(w, r, "/", http.StatusInternalServerError)
				return
			}

			c := &http.Cookie{
				Path:  "/",
				Name:  auth.Cookiename,
				Value: token,
			}

			d.LoggedIn = true
			d.LoginFailure = false

			auth.AddCookie(token, username)

			http.SetCookie(w, c)

			http.Redirect(w, r, "/", http.StatusFound)
		}

		d.LoginFailure = true
	}

	err := templ.Execute(w, d)
	if err != nil {
		fmt.Println(err)
	}

}
